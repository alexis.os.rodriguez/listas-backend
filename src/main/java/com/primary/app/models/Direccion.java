package com.primary.app.models;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "direcciones")
public class Direccion implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8232302611180163340L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "persona_id", referencedColumnName = "id")
	private Persona personaId;
	private String calle;
	private String ciudad;
	private String pais;
	@Column(name ="codigo_postal")
	private Integer codigoPostal;
	private Integer altura;
	private String provincia;
	
	
	
	public Direccion() {
	}
	
	
	
	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Persona getPersonaId() {
		return personaId;
	}
	public void setPersonaId(Persona personaId) {
		this.personaId = personaId;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public Integer getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(Integer codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public Integer getAltura() {
		return altura;
	}
	public void setAltura(Integer altura) {
		this.altura = altura;
	}



	@Override
	public String toString() {
		return "Direccion [id=" + id + ", personaId=" + personaId.getId() + ", calle=" + calle + ", ciudad=" + ciudad
				+ ", pais=" + pais + ", codigoPostal=" + codigoPostal + ", altura=" + altura + ", provincia="
				+ provincia + "]";
	}
	
	
	
	
	
	
}
