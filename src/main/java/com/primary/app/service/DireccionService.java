package com.primary.app.service;

import java.util.List;

import com.primary.app.dto.CodigoPostalDto;
import com.primary.app.dto.DireccionDto;
import com.primary.app.models.Direccion;

public interface DireccionService  {

	public DireccionDto direccionPorPersona(Long id);
	
	public List<CodigoPostalDto> listarDirecciones();

	
}
