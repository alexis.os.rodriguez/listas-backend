package com.primary.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.primary.app.models.Persona;
import com.primary.app.repository.IPersonaRepository;

@Service
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	private IPersonaRepository personaRepository;
	
	@Override
	public List<Persona> findAll() {
		return personaRepository.findAll();
	}

	@Override
	public Persona findById(Long id) {
		return personaRepository.findById(id).orElse(null);
	}

}
