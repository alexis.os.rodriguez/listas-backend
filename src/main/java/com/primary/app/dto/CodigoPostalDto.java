package com.primary.app.dto;

import javax.persistence.Column;

public class CodigoPostalDto {
	
	private Integer codigoPostal;
	private String calle;
	private String ciudad;
	private String provincia;
	
	
	
	public CodigoPostalDto(Integer codigoPostal, String calle, String ciudad, String provincia) {
		this.codigoPostal = codigoPostal;
		this.calle = calle;
		this.ciudad = ciudad;
		this.provincia = provincia;
	}
	
	public Integer getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(Integer codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	
}
