package com.primary.app.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.primary.app.dto.CodigoPostalDto;
import com.primary.app.dto.DireccionDto;
import com.primary.app.models.Direccion;
import com.primary.app.models.Persona;
import com.primary.app.repository.IDireccionRepository;
import com.primary.app.repository.IPersonaRepository;

@Service
public class DireccionServiceImpl implements DireccionService {

	@Autowired
	private IPersonaRepository personaRepository;

	@Autowired
	private IDireccionRepository direccionRepository;

	@Override
	public DireccionDto direccionPorPersona(Long id) {

		Persona persona = personaRepository.findById(id).orElse(null);

		Direccion direccion = direccionRepository.findByPersonaId(persona);

		DireccionDto direccionDto = new DireccionDto();
		
		direccionDto.setAltura(direccion.getAltura());
		direccionDto.setCalle(direccion.getCalle());
		direccionDto.setCiudad(direccion.getCiudad());
		direccionDto.setPais(direccion.getPais());
		direccionDto.setPersonaId(direccion.getPersonaId().getId());
		direccionDto.setProvincia(direccion.getProvincia());
		

		return direccionDto;
	}

	@Override
	public List<CodigoPostalDto> listarDirecciones() {
		
		List<Direccion> direcciones = direccionRepository.findAll();
		
		List<CodigoPostalDto> cps = new ArrayList<>();
		
		for(Direccion d: direcciones) {	
			cps.add(new CodigoPostalDto(d.getCodigoPostal(), d.getCalle(), d.getCiudad(), d.getProvincia()));
			
		}
		
		return cps;
	}

}
