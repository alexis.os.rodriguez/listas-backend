package com.primary.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.primary.app.models.Direccion;
import com.primary.app.models.Persona;

public interface IDireccionRepository extends JpaRepository<Direccion, Long> {
	
	public Direccion findByPersonaId(Persona id);
	
}
