package com.primary.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.primary.app.models.Persona;
import com.primary.app.service.PersonaService;

@RestController
@CrossOrigin(origins = "http://localhost:4200") //  the annotation enables Cross-Origin Resource Sharing (CORS) on the server.
public class PersonaController {

	@Autowired
	private PersonaService personaService;
	
	
	@GetMapping("/listPersonaA")
	public List<Persona> listar(){
		return personaService.findAll();
	}
	
	@GetMapping("/listPersonaB")
	public List<Persona> listarB(){
		return personaService.findAll();
	}
	
	
}
