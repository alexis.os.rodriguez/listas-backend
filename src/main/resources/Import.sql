INSERT INTO personas(nombre, apellido) VALUES('Alexis', 'Rodriguez');
INSERT INTO personas(nombre, apellido) VALUES('Tomas', 'Pereyra');
INSERT INTO personas(nombre, apellido) VALUES('Juan', 'Perez');

INSERT INTO direcciones(persona_id, calle, ciudad, pais, codigo_postal, altura, provincia) VALUES(1,'Ezequiel Paz', 'Temperley', 'Argentina', 1834, 224, 'Buenos Aires');
INSERT INTO direcciones(persona_id, calle, ciudad, pais, codigo_postal, altura, provincia) VALUES(2,'alberdi', 'Flores', 'Argentina', 1834, 224, 'Buenos Aires');
INSERT INTO direcciones(persona_id, calle, ciudad, pais, codigo_postal, altura, provincia) VALUES(3,'Rivadavia', 'Caballito', 'Argentina', 1834, 224, 'Buenos Aires');
