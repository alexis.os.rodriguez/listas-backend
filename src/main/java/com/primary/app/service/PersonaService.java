package com.primary.app.service;

import java.util.List;

import com.primary.app.models.Persona;

public interface PersonaService {
	
	public List<Persona> findAll();
	
	public Persona findById(Long id);

}
