package com.primary.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.primary.app.models.Persona;

public interface IPersonaRepository extends JpaRepository<Persona, Long> {

	
	
}
