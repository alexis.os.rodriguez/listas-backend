package com.primary.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootExamenPrimaryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootExamenPrimaryApplication.class, args);
	}

}
